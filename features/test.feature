@NIG-4
Feature: Iselect page transition execution

	@TEST_NIG-1 @TESTSET_NIG-2
	Scenario Outline: Iselect initial page transition
		Given User opens URL "https://energy.iselect.com.au/electricity/"
		    When User enters Postcode as "2000"
		    And Select the "2000, SYDNEY" option from the dropdown
		    And Click on start
		    Then Page title should be "iSelect - Electricity Comparison | Gas Rates | Compare Energy Rates"
		    When User selects "<productType>","<propertyType>","<propertyOwnership>","<moveIn>"
		    And User selects electricty bill as "<electricitybill>" and gas bill as "<gasbill>" 
		    And User selects solarpanels as "<solarpanel>", electricity provider as "<elecprovider>", electricity usage as "<elecusage>"
		    And User selects gas provider as "<gasprovider>", gas usage as "<gasusage>"
		    And User checks the two agreements
		    And User clicks on continue
		    Then Page title should be "iSelect - Your Details"
		    
		Examples:
				| productType | propertyType | propertyOwnership | moveIn | electricitybill | gasbill | solarpanel | elecprovider | elecusage | gasprovider | gasusage |    
				| utilitiesbundle | RES | Owner | compareplans | 3 | 3 | Yes | AGL | Low | ActewAGL | Med |

